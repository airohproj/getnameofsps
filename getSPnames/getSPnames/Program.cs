﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace getSPnames
{
    class Program
    {
        static private string[] _files;
        static void Main(string[] args)
        {
            // check if script is called with 2 arguments
            if (args.Count() != 2)
            {
                Console.WriteLine("Usage: getSPname <path_to_cpp_files> <path_to_output_file");
                return;
            }

            // chech if directory exists
            if (!Directory.Exists(args[0]))
            {
                Console.WriteLine("Directory doesn't exist!");
                return;
            }

            // check if output file exists, else create it
            if (!File.Exists(args[1]))
            {
                File.Create(args[1]).Close();
            }
            
            // get files from folder
            _files = Directory.GetFiles(args[0]);

            // loop true each file
            foreach (var file in _files)
            {
                if (Path.GetExtension(file) != ".cpp")
                    continue;

                string[] lines;

                try
                {
                    lines = GetFileContent(file);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    continue;
                }
                
                SaveRelevantContentToOutput(lines, args[1]);
            }
        }

        private static void SaveRelevantContentToOutput(string[] lines, string output)
        {
            foreach (string line in lines)
            {
                // search each line for expression
                var match = Regex.Match(line, @"EXEC(?<pName>[^0-9\n]*)@");

                // save in output file if match is found in line
                if (match.Success)
                {
                    string procedureName = match.Groups["pName"].Value;
                    using (StreamWriter sw = File.AppendText(output))
                    {
                        sw.WriteLine(procedureName);
                    }
                }
            }   
        }

        private static string[] GetFileContent(string file)
        {
            if (!File.Exists(file))
            {
                throw new FileNotFoundException("File " + file + " not found!");
            }

            return System.IO.File.ReadAllLines(file);
        }
    }
}
